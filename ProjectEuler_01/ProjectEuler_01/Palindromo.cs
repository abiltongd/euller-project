﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class Palindromo
    {
        public static void Main(string[] args)
        {
            int n = 986;
            IsPalidromo(n);

            try
            {
                if (!IsPalidromo(999)) throw new Exception("Falha na implementação. 999 é um palíndromo");
                if (!IsPalidromo(989)) throw new Exception("Falha na implementação. 989 é um palíndromo");
                if (!IsPalidromo(898)) throw new Exception("Falha na implementação. 898 é um palíndromo");
                if (IsPalidromo(997)) throw new Exception("Falha na implementação. 997 não é um palíndromo");
                if (IsPalidromo(977)) throw new Exception("Falha na implementação. 977 não é um palíndromo");
                if (IsPalidromo(799)) throw new Exception("Falha na implementação. 799 não é um palíndromo");
            } catch (Exception e)
            {
                Console.ReadKey();
            }

            /*
            999 * 999
            999 * 998
            998 * 998
            998 * 997
            997 * 997
            997 * 996
            996 * 996
            996 * 995
            995 * 995
               */

            var maior = 0;
            int a = 0, b = 0;
            for ( int i = 999; i >= 100; i--)
            { 
                for (int j = 999; j >= 100; j--)
                {
                    var num = i * j;
                    if (IsPalidromo(num)  && maior < num)
                    {
                        a = i;
                        b = j;
                        maior = num;
                    }
                }
            }

            Console.WriteLine(maior+ " = " + a + " * " + b);
            Console.ReadKey();


        }

        static bool IsPalidromo(int n)
        {
            string convertString = Convert.ToString(n);
            char[] arrChar = convertString.ToCharArray();
            Array.Reverse(arrChar);
            string invertida = new string(arrChar);
            return invertida.CompareTo(convertString) == 0;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class Divisiveis
    {
        public static void Main(string[] args)
        {
            bool limite = false;
            int n = 20;

            while (limite != true)
            {
              
                if (IsDivisible(n))
                {
                    Console.WriteLine("O número é: " + n);
                    Console.ReadKey();
                    limite = true;
                }

                n+=10;
                limite = false;


            }
        }
        static bool IsDivisible(int n)
        {
            var maior = 0;
            if (n > maior)
            {
                for (int i = 2; i <= 20; i++)
                {
                    if (n % i != 0)
                    {
                        return false;
                    }

                    if (n % i == 0 || n >= maior)
                    {
                        maior = n;
                    }
                }
                return true;
            }
            return false;
        }
    }
}

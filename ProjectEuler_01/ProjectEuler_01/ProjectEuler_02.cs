﻿using System;

namespace ProjectEuler_01
{
    public class ProjectEuler_02
    {
        public static void Main(string[] s)
        {
            if (Fibonnacci(1) != 1 ||
                Fibonnacci(2) != 2 ||
                Fibonnacci(3) != 3 ||
                Fibonnacci(4) != 5 ||
                Fibonnacci(5) != 8 ||
                Fibonnacci(6) != 13 ||
                Fibonnacci(7) != 21 ||
                Fibonnacci(8) != 34 ||
                Fibonnacci(9) != 55 ||
                Fibonnacci(10) != 89
                )
            {
                throw new Exception("Erro na implementação do método");
            }

            long soma = 0;
            long index = 1, valorItem = 0;
            while(valorItem < 4000000)
            {
                Console.WriteLine("Valor do Item: " + valorItem);
                if (valorItem % 2 == 0)
                    soma += valorItem;
                Console.WriteLine("Soma: " + soma);
                valorItem = Fibonnacci(index++);
                
            }
            Console.WriteLine(soma);
            Console.ReadKey();
        }

        static long Fibonnacci(long n)
        {
            var a1 = 1;
            var a2 = 2;
            var proximo = 0;

            if (n > 2)
            {
                for (int i = 3; i <= n; i++)
                {
                    proximo = a1 + a2;
                    a1 = a2;
                    a2 = proximo;
                    
                }
                return proximo;
            }
            
            return n;

        }

    }
}

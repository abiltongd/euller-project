﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class Longest_Collatz_Sequence
    {
        public static void Main(string[] args)
        {
            const long MAX = 1000000;
            long contain = 1, sum = 0, resultado = 0, resultadoCalculado = 0, calculador = 0;
            for (long i = MAX; i >= 1; i--)
            {
                contain = 1;
                calculador = i;
                do
                {
                    resultadoCalculado = IsEvenEvenOrOdd(calculador);
                    calculador = resultadoCalculado;
                    contain += 1;
                } while (calculador != 1);
                if (contain > sum)
                {
                    resultado = i;
                    sum = contain;
                }
            }
            Console.WriteLine("O número " + resultado + " teve " + sum + " termos!");
            Console.ReadKey();
        }
        static long IsEvenEvenOrOdd(long n)
        {
            if (n % 2 == 0)
            {
                n = n / 2;
                return n;
            }

            n = 3 * n + 1;
            return n;
        }
    }
}

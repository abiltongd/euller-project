﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class NumerosPrimos_Problema07
    {
        public static void Main(string[] args)
        {
            int n = 2, soma = 0;
            int resultado = proximoPrimo(n, soma);
            Console.WriteLine("Número de posição 10001 o número primo é: " + resultado);
            Console.ReadLine();
        }

        static int proximoPrimo(int primo, int soma)
        {
            bool parar = false;
            while (parar != true)
            {
                if (IsPrime(primo))
                {
                    soma += 1;
                }
                if (soma == 10001)
                {
                    parar = true;
                    return primo;
                }

                primo += 1;
                parar = false;
            }
            return -1;
        }
        static bool IsPrime(int n)
        {
            for (int i = 2; i <= n / 2; i++)
            {
                if (n % i == 0)
                    return false;
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class NumerosPrimos
    {
        public static void Main(string[] a)
        {
            long numero = 600851475143;

            bool pare = false;
            long primo = 2;
            while (!pare)
            {
                if (numero % primo == 0)
                {
                    numero = numero / primo;
                    Console.WriteLine("Fatorado: " + numero);
                }
                else
                {
                    primo = proximoPrimo(primo, numero / 2);
                    if (primo == -1)
                    {
                        Console.WriteLine("Fim do processamento");
                        Console.ReadLine();
                        return;
                    }
                }
                if (numero == 1)
                {
                    Console.WriteLine("Fim do processamento. Encontrado: " + primo);
                    Console.ReadLine();
                    return;
                }
            }
        }

        static long proximoPrimo(long primo, long limite)
        {
            long proximoPrimo = primo + 1;
            while (proximoPrimo < limite)
            {
                if (ehPrimo(proximoPrimo))
                {
                    return proximoPrimo;
                }
                proximoPrimo++;
            }
            return -1;
        }

        static bool ehPrimo(long n)
        {
            for (long i = 2; i < n / 2; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}

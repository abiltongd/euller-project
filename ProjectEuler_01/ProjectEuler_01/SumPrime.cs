﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class SumPrime
    {
        const long LIMITE = 2000000;
        public static void Main(string[] args)
        {
            long sum = 0;
            for (long i = 2; i < LIMITE ; i++)
            {
                if (isPrime(i))
                {
                    Console.WriteLine(i);
                     sum +=  i;  
                }
            }

            Console.WriteLine(sum);
            Console.ReadKey();
        }

        static bool isPrime(long n)
        {
            for (long i = 2; i <= n / 2; i++)
            {
                if (n % i == 0)
                    return false;
            }
            return true;
        }

        
    }
}

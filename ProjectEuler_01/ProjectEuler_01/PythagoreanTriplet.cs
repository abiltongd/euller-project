﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectEuler_01
{
    public class PythagoreanTriplet
    {
        public static void Main(string[] args)
        {
            for (double a = 1; a < 500; a++)
            {
                for (double b = a + 1; b < 500; b++)
                {
                    var c = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b,2));
                    if ((a + b + c) == 1000)
                    {
                        Console.WriteLine(a * b * c);
                        Console.ReadKey();
                        return;
                    }
                }
            }
        }
    }
}
